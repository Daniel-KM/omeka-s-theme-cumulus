'use strict';

$(document).ready(function() {

    const $contactUs = $("#contact-us");

    const $contactStoreAdd = $("#contact-us-store-add");
    const $contactStoreClear = $("#contact-us-store-clear");

    function contactForm() {
        $contactUs.children("#submit").hide();

        $("#consent").on("click", function (evt) {
            $("#contact-us > #submit").toggle(evt.target.checked);
        });

        // Add hidden inputs for stored items.
        var items = sessionStorage.getItem('contact-us-store');
        if (items) {
            items.split('|').forEach(function(value) {
                $contactUs.append(
                    '<input type="hidden" form="contact-us" name="fields[id][]" value="' + value + '"/>'
                );
            });
        }
    }

    $contactStoreAdd.on('click', function () {
        const checkedItems = [...document.querySelectorAll('.search-results .contact-us-resource:checked')].map(e => e.value);
        if (checkedItems.length) {
            var items = sessionStorage.getItem('contact-us-store');
            items = items ? items.split('|') : [];
            // Merge and deduplicate.
            var newItems = [...new Set([...items, ...checkedItems])];
            sessionStorage.setItem('contact-us-store', newItems.join('|'));
        }
    });

    $contactStoreClear.on('click', function (e) {
        sessionStorage.removeItem('contact-us-store');
    });

    $(function () {
        $contactUs.length > 0 && contactForm();
    });
});
